let posts = []
let count = 1

// show the posts
const showPosts = (posts) => {
		let post_entries = ""

		posts.forEach(post  => {
			post_entries += `
				<div id="post-${post.id}">
				    <h3 id="post-title-${post.id}">${post.title}</h3>
				    <p id="post-body-${post.id}">${post.body}</p>
				    <button onclick="editPost('${post.id}')">Edit</button>
				    <button onclick="deletePost('${post.id}')">Delete</button>
				</div> 
			`
		})
		document.querySelector('#div-post-entries').innerHTML = post_entries
}

// Add new post
document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	event.preventDefault()

	posts.push({
		// Temporary id as replacement for actual DB id that is auto generated
		id: count, 
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})

	count++
	showPosts(posts)
	// alert('Successfully added a new Post!')
})

// Editing a post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}

const deletePost = (id) => {
	// Option 1
    // for (let i = 0; i < posts.length; i++) {

    //     if (posts[i].id.toString() === id) {
	 		// posts.splice(i,1)
    //         showPosts(posts);  
    //         break;
    //     }
    // }

    // Option 2
    posts = posts.filter(post => {
    	return post.id.toString() != id
    })

    showPosts(posts);  


}


// updating a post
document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
    event.preventDefault();

    for (let i = 0; i < posts.length; i++) {
        // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
        // Therefore, it is necesary to convert the Number to a String first.

        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;
    
            showPosts(posts);
            // alert('Successfully updated.');
            
            break;
        }
    }
});

